﻿namespace QuienEstaHablando
{
    using Microsoft.ML.Data;
    public class Dialogo
    {
        [LoadColumn(0)]
        public string Hablante { get; set; }


        [LoadColumn(1)]
        public string Mensaje { get; set; }
    }
}
