﻿namespace QuienEstaHablando
{
    public class ResultadoPrediccion
    {
        public bool PredictedLabel { get; set; }
        public float Probability { get; set; }
        public float Score { get; set; }
    }
}
