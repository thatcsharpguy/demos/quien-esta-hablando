﻿namespace QuienEstaHablando
{
    using Microsoft.ML;
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            var contexto = new MLContext();

            var todosLosDatos = contexto.Data.LoadFromTextFile<Dialogo>(
                "quien_esta_hablando.csv",
                separatorChar: ',',
                hasHeader: true
            );

            var conjuntos = contexto.Data.TrainTestSplit(
                data: todosLosDatos,
                testFraction: 0.2
            );

            var transformacionTexto = contexto.Transforms.Text.FeaturizeText(
                outputColumnName: "Features",
                inputColumnName: nameof(Dialogo.Mensaje)
            );

            var mapaBooleano = contexto.Data.LoadFromEnumerable(new[] {
                new { Entrada = "amlo", Valor = true },
                new { Entrada = "lopez_gatell", Valor = false },
            });

            var transformacionEtiqueta = contexto.Transforms.Conversion.MapValue(
                outputColumnName: "Label",
                lookupMap: mapaBooleano,
                keyColumn: mapaBooleano.Schema["Entrada"],
                valueColumn: mapaBooleano.Schema["Valor"],
                inputColumnName: nameof(Dialogo.Hablante)
            );

            var clasificador = contexto.BinaryClassification.Trainers.SdcaLogisticRegression();

            var pipeline = transformacionTexto
                .Append(transformacionEtiqueta)
                .Append(clasificador);

            var modeloEntrenado = pipeline.Fit(conjuntos.TrainSet);

            var evaluacion = contexto.BinaryClassification.Evaluate(
                modeloEntrenado.Transform(conjuntos.TestSet)
            );
            
            Console.WriteLine($"Tu algoritmo es {evaluacion.Accuracy * 100}% correcto");

            // En lugar de estar entrenando el modelo constantemente, podemos leer el que ya entrenamos
            // y realizar predicciones con él: 

            var modeloGuardado = contexto.Model.Load("modelo.zip", out var schema);

            var motorPrediccion = contexto.Model.CreatePredictionEngine<Dialogo, ResultadoPrediccion>(modeloGuardado);

            ResultadoPrediccion resultado = motorPrediccion.Predict(new Dialogo 
            { 
                Mensaje = args[0]
            });

            Console.WriteLine($"Lo dijo el presidente {resultado.PredictedLabel} {resultado.Probability}");
            
        }
    }
}
